﻿using System;
using UnityEngine;

public enum EEnemyState{
    Sleep,
    Wait,
    StartWalk,
    Walk,
    StartAttack,
    Attack
    
}

public class EnemyMove : MonoBehaviour
{

    [SerializeField] private Animator m_animator;
    [SerializeField] private Transform m_chekGroundPoint;
    [SerializeField] private Transform m_chekAttackPoint;
    [SerializeField] private Transform m_graphics;

    private EEnemyState _currentState = EEnemyState.Sleep;

    private Player _player;
    

    private float wakeUpTimer;
    private EEnemyState nextState;
    private float waitTimer;
    private float attackTimer;
    private float currentDirection = 1f;

    private void Awake() {
        wakeUpTimer = Time.time + 1f;
        _player = FindObjectOfType<Player>();
    }

    void Start()
    {
        
    }
    
    void Update()
    {
        switch(_currentState) {
            case EEnemyState.Sleep 
                : break;
            case EEnemyState.Wait 
                : break;
            case EEnemyState.StartWalk 
                : break;
            case EEnemyState.Walk 
                : break;
            case EEnemyState.StartAttack 
                : break;
            case EEnemyState.Attack 
                : break;
            default : throw new ArgumentOutOfRangeException();
        }
    }

    private void Sleep() {
        
    }

    private void Wait() {
        
    }

    private void Walk() {
        
    }

    private void Atack() {
        
    }
}
